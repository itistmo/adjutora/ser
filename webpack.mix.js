let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix
    .js('resources/assets/js/app.js', 'public/js')
    .js('resources/assets/js/material/core/jquery.min.js','public/js/material/core')
    .js('resources/assets/js/material/core/popper.min.js','public/js/material/core')
    .js('resources/assets/js/material/bootstrap-material-design.js','public/js/material')
    .js('resources/assets/js/material/material-dashboard.js','public/js/material')
    .js('resources/assets/js/material/material-kit.js','public/js/material')
    .js('resources/assets/js/material/system.js','public/js/material/system.js')
    .js('resources/assets/js/material/plugins/arrive.min.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/bootstrap-datetimepicker.min.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/bootstrap-notify.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/bootstrap-selectpicker.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/bootstrap-tagsinput.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/bootstrap.min.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/chartist.min.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/demo.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/jasny-bootstrap.min.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/jquery.flexisel.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/material.min.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/nouislider.min.js','public/js/material/plugins')
    .js('resources/assets/js/material/plugins/perfect-scrollbar.jquery.min.js','public/js/material/plugins')
    .sass('resources/assets/sass/material/dashboard/material-dashboard.scss', 'public/css')
    .sass('resources/assets/sass/material/kit/material-kit.scss', 'public/css')
    .version()
    .sourceMaps()
    .browserSync({
        proxy: 'https://ser.app',
        files: [
            'app/**/*',
            'resources/views/**/*',
            'resources/lang/**/*',
            'routes/**/*'
        ]
    });
